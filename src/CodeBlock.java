import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;


public class CodeBlock {

    ArrayList<String> code;
//    HashMap<String,CompilerFrame> frames;
    Stack<CompilerFrame> frames;
    CompilerFrame currentFrame;
    Stack<File> files;
    int frameCounter;
    
    
    File demo;

    public CodeBlock(){
        code = new ArrayList(50);
        frames = new Stack();
        currentFrame = null;
        files = new Stack();
        frameCounter=1;
        File file = new File("frame.j");
        demo = new File("Demo.j");
        dumpHeader();
		// if file doesnt exists, then create it
		if (!file.exists()) {
			try {
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(".source frame.j");
				bw.newLine();
				bw.write(".interface public frame");
				bw.newLine();
				bw.write(".super java/lang/Object");
				bw.newLine();	
				bw.flush();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			
		}
    }

    void emit_push(int val){
        code.add("       sipush "+val);
    }

    void emit_add(){

        code.add("       iadd");
    }

    void emit_sub(){

        code.add("       isub");
    }

    void emit_mul(){

        code.add("       imul");
    }

    void emit_div(){

        code.add("       idiv");
    }
    
    void beginScope(CompilerFrame frame){
    	frame.setFrameCount(frameCounter);
    	frames.push(currentFrame);
    	currentFrame = frame;
    	frameCounter++;
    	
    	String frameName = currentFrame.getFrameName();
    	
    	File file = new File(frameName+".j");
    	files.push(file);

		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(file));
   	
			bw.write(".source "+frameName+".j");
			bw.newLine();
			bw.write(".class "+frameName);
			bw.newLine();
			bw.write(".super java/lang/Object");
			bw.newLine();
			bw.write(".implements frame");
			bw.newLine();
			bw.newLine();
			if(currentFrame.getAncestor().hasAncestor()){
				bw.write(".field public SL Lframe_"+currentFrame.getAncestor().getFrameCount()+";");
				bw.newLine();
				bw.newLine();
			}			
			bw.flush();
        	bw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		; create a stack frame
		code.add("       new "+frameName);
		
//		; save the reference for later
		code.add("       dup");
		
//		; call the object constructor
		code.add("       invokespecial "+frameName+"/<init>()V");
		
//		; save the reference for later
		code.add("       dup");
		
		
		if(currentFrame.getAncestor().hasAncestor()){
			aload_1();
			code.add("       putfield "+frameName+"/SL L"+currentFrame.getAncestor().getFrameName()+";");
			code.add("       dup");	
		}
    }
    
    
    void endScope(){
    	
    	String frameName = "frame_"+frameCounter;
    	   	
    	BufferedWriter bw;
    	File file = files.pop();
		try {
			bw = new BufferedWriter(new FileWriter(file.getName(),true));
			   	
			bw.newLine();
			bw.write(".method public <init>()V");
			bw.newLine();
			bw.write("aload_0");
			bw.newLine();
			bw.write("invokespecial java/lang/Object/<init>()V");
			bw.newLine();
			bw.write("return");
			bw.newLine();
			bw.write(".end method");
			bw.flush();
        	bw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		currentFrame = currentFrame.getAncestor();
//		currentFrame = frames.pop();
		if(currentFrame.getAncestor().hasAncestor()){
			aload_1();
	    	checkcast(currentFrame.getFrameName());			
			code.add("       getfield "+currentFrame.getFrameName()+"/SL L"+currentFrame.getAncestor().getFrameName()+";");	
			astore_1();
			currentFrame = frames.pop();
		}
			
    }
    
    void astore_1(){
    	code.add("       astore 1");
    }
    
    void aload_1(){
    	code.add("       aload 1");
    }
    
    void checkcast(String framename){
    	code.add("       checkcast "+framename);
    }
    
    void getfield(String field){
    	code.add("       getfield "+field);
    }
    
    void dup(){
//		dup ; save the reference for later
		code.add("       dup");
    }
    
    void assoc(String id){
    	BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(files.peek().getName(),true));
			bw.write(".field public "+id+" I");
			bw.newLine();
			bw.flush();
        	bw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		
//		id.getVal().compile(code, frame); from Codeblock
//		putfield frame_1/loc_0 I 	; store the value for id x
		code.add("       putfield "+currentFrame.getFrameName()+"/"+id+" I");
    }
    

    void dumpHeader(){
		code.add(".class public Demo");
    	code.add(".super java/lang/Object");
    	code.add("");
    	code.add(";");	    	 
    	code.add("; standard initializer");	    	 
    	code.add(".method public <init>()V");    	 
    	code.add("   aload_0");	    	 
    	code.add("   invokenonvirtual java/lang/Object/<init>()V");	    	 
    	code.add("   return");	    	 
    	code.add(".end method");
    	code.add("");
    	code.add(".method public static main([Ljava/lang/String;)V");	    	 
    	code.add("       ; set limits used by this method");	    	 
    	code.add("       .limit locals 2");	    	 
    	code.add("       .limit stack 256");
    	code.add("");
    	code.add("       ; setup local variables:");
    	code.add("");
    	code.add("       ;	1 - the PrintStream object held in java.lang.System.out");
    	code.add("       getstatic java/lang/System/out Ljava/io/PrintStream;");
    	code.add("");
    	code.add("       ; place your bytecodes here");
    	code.add("       ; START");
    	code.add("");
    }

    void dumpFooter(){
    	
    	code.add("       aconst_null");
    	code.add("       astore 1");
		code.add("");
		code.add("       ; END");
    	code.add("");
    	code.add("");
    	code.add("       ; convert to String;");         
    	code.add("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");        
    	code.add("       ; call println");         
    	code.add("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        code.add("");
        code.add("       return");
        code.add("");
        code.add(".end method");
    }

    void dumpCode(){
    	dumpFooter();
    	BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(demo));
		
	        for(String c : code){
	        	bw.write(c);
	        	bw.newLine();
	            System.out.println(c);
	        }
			bw.flush();
	    	bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void dump(){
//        dumpHeader();
        dumpCode();
//        dumpFooter();
    }

	
}