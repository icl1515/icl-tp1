import java.util.ArrayList;

public class ASTDecl implements ASTNode {
	
	ArrayList<Binding> ids;
    ASTNode body;

    public ASTDecl(ArrayList<Binding> ids, ASTNode body){
        this.ids = ids;
        this.body = body;
        
    }

    public int eval(Environ env) {
        int value;
        int idValue;
        

        Environ newEnv = env.beginScope();
        for (Binding id:ids){
        	idValue = id.getVal().eval(env);
        	newEnv.assoc(id.getId(),idValue);
        }
        value = body.eval(newEnv);
        
        newEnv.endScope();

        return value;
    }

    public String to_string() {
        return null;
    }

    public String to_AST() {
        return null;
    }

    public void compile(CodeBlock code,CompilerFrame frame) {
        int var = 0;

        CompilerFrame newFrame = frame.beginScope();
        code.beginScope(newFrame);
        
        var = 0;
        
        for (Binding id:ids){//????
        	id.getVal().compile(code, frame);
        	newFrame.assoc(id.getId(),var);
        	code.assoc(id.getId());// associa a var � framestack 
        	var++;
        	if(var < ids.size())
        		code.dup();
        	else
        		code.astore_1();
        	
        }
        body.compile(code, newFrame);
        code.endScope();
        
        
        //value = body.eval(newEnv);
               
        newFrame.endScope();


    }
}
