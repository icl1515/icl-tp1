public interface ASTNode {

	public int eval(Environ env);

	public void compile(CodeBlock code,CompilerFrame env);

}

