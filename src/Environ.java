import java.util.TreeMap;

public class Environ {

    Environ up;
    TreeMap<String,Integer> ids = new TreeMap();

    public Environ(){
        this.up = null;
    }

    private Environ(Environ up){
        this.up = up;
    }

    public int find(String id)throws UndeclaredIdentifierExecption{
        if( up == null )
            throw new UndeclaredIdentifierExecption(id);
        assert (!this.ids.isEmpty());
        if( this.ids.containsKey(id) )
            return this.ids.get(id);
        else
        	return up.find(id);
    }

    public Environ beginScope(){
        return  new Environ(this);
    }

    public Environ endScope(){
        return  up;
    }

    public void assoc(String id, int val){
    	this.ids.put(id, val);
    }
}
