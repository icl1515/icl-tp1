
public class ASTDiv implements ASTNode {

	ASTNode left, right;
	public int eval(Environ env) { 
		return left.eval(env) / right.eval(env); 
	}

    public ASTDiv(ASTNode l, ASTNode r)
    {
    	left = l; right = r;
    }


	public void compile(CodeBlock code,CompilerFrame env) {
		left.compile(code,env);
		right.compile(code,env);
		code.emit_div();
	}

}
