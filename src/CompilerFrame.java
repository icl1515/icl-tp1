import java.util.TreeMap;

public class CompilerFrame {
	
	private CompilerFrame up;
	private int frameCount;
	private TreeMap<String,Integer> ids = new TreeMap();
	
	CompilerFrame(){
		this.up = null;
	}
	
	CompilerFrame(CompilerFrame up){
		this.up = up;
	}
	
	public void find(String id,CodeBlock code, boolean firstTime)throws UndeclaredIdentifierExecption{
		TreeMap<String,Integer> frameids = this.ids;
		if(firstTime){
			code.aload_1();
			code.checkcast(this.getFrameName());
		}
		
        if( up == null )
            throw new UndeclaredIdentifierExecption(id);
        assert (!this.ids.isEmpty()); 
        
//        while(!this.ids.containsKey(id)){
//        	code.getfield(this.getFrameName()+"/SL L"+this.getAncestor().getFrameName());
//        	frameids = 
//        	up.find(id,code);
//        }
//        code.getfield(this.getFrameName()+"/loc_"+this.ids.get(id)+" I");
//        this.ids.get(id);
        
        if( this.ids.containsKey(id) ){
//        	getfield frame_1/loc_1 I
        	code.getfield(this.getFrameName()+"/"+id+" I");
            this.ids.get(id);
        }else{
//        	getfield frame_2/SL Lframe_1;
        	code.getfield(this.getFrameName()+"/SL L"+this.getAncestor().getFrameName()+";");
        	up.find(id,code,false);
        }
    }
	
	public String getFrameName(){
		return "frame_"+frameCount;
	}
	
	public int getFrameCount(){
		return frameCount;
	}
	
	public void setFrameCount(int fc){
		this.frameCount = fc;
	}
	
	public boolean hasAncestor(){
		return up != null;
	}
	
	public CompilerFrame getAncestor(){
		return up;
	}

    public CompilerFrame beginScope(){
        return  new CompilerFrame(this);
    }

    public CompilerFrame endScope(){
        return  up;
    }

    public void assoc(String id, int val){
    	this.ids.put(id, val);
    }

}
