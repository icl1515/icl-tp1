
public class ASTNeg implements ASTNode {
    ASTNode val;

    public ASTNeg(ASTNode v){
        val = v;
    }

    public int eval(Environ env) {
        return (-1) * val.eval(env);
    }
/*
    public String to_string() {
        return "-("+val.to_string()+")";
    }

    public String to_AST() {
        return "neg("+val.to_AST()+")";
    }
*/
    public void compile(CodeBlock code,CompilerFrame env) {
        val.compile(code,env);
        code.emit_push(-1);
        code.emit_mul();
    }
}
