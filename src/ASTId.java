public class ASTId implements ASTNode {
    String id;

    public ASTId(String id){
        this.id = id;
    }

    public int eval(Environ env) {
        try {
			return env.find(id);
		} catch (UndeclaredIdentifierExecption e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return 0;
    }

    public String to_string() {
        return null;
    }

    public String to_AST() {
        return null;
    }

    public void compile(CodeBlock code, CompilerFrame env) {
    	try {
			env.find(id,code,true);
		} catch (UndeclaredIdentifierExecption e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}