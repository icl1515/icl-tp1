public class ASTNum implements ASTNode {

	int val;

	public int eval(Environ env) { return val; }

    public ASTNum(int n)
    {
		val = n;
    }
    
	public void compile(CodeBlock code,CompilerFrame env) {
		code.emit_push(val);
	}
}

