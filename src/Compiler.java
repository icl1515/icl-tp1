
public class Compiler {

	public static void main(String[] args) {
		Parser parser = new Parser(System.in);
	    ASTNode exp;
	    Environ env;
	    CompilerFrame cfEnv;
	    CodeBlock cb;

	    
	    try {
		    
		    env = new Environ();
		    cb = new CodeBlock();
		    cfEnv = new CompilerFrame();
		    exp = parser.Start();
		    //System.out.println( exp.eval(env) );
		    exp.compile(cb,cfEnv);
		    cb.dump();
	    } catch (Exception e) {
	    	System.out.println ("Syntax Error!");
	    	e.printStackTrace();
	    	parser.ReInit(System.in);
	    }
	    

	}

}
