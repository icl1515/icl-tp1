public class Binding {
    String id;
    ASTNode val;
    
	public Binding(String id, ASTNode val) {
		super();
		this.id = id;
		this.val = val;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ASTNode getVal() {
		return val;
	}
	public void setVal(ASTNode val) {
		this.val = val;
	}
    


}
