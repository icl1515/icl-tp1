.class public Demo
.super java/lang/Object

;
; standard initializer
.method public <init>()V
   aload_0
   invokenonvirtual java/lang/Object/<init>()V
   return
.end method

.method public static main([Ljava/lang/String;)V
       ; set limits used by this method
       .limit locals 2
       .limit stack 256

       ; setup local variables:

       ;	1 - the PrintStream object held in java.lang.System.out
       getstatic java/lang/System/out Ljava/io/PrintStream;

       ; place your bytecodes here
       ; START

       new frame_1
       dup
       invokespecial frame_1/<init>()V
       dup
       sipush 1
       putfield frame_1/z I
       astore 1
       new frame_2
       dup
       invokespecial frame_2/<init>()V
       dup
       aload 1
       putfield frame_2/SL Lframe_1;
       dup
       new frame_3
       dup
       invokespecial frame_3/<init>()V
       dup
       aload 1
       putfield frame_3/SL Lframe_1;
       dup
       sipush 1
       putfield frame_3/x I
       astore 1
       aload 1
       checkcast frame_3
       getfield frame_3/x I
       sipush 1
       iadd
       aload 1
       checkcast frame_3
       getfield frame_3/SL Lframe_1;
       astore 1
       putfield frame_2/x I
       astore 1
       aload 1
       checkcast frame_2
       getfield frame_2/x I
       aload 1
       checkcast frame_2
       getfield frame_2/SL Lframe_1;
       getfield frame_1/z I
       iadd
       new frame_4
       dup
       invokespecial frame_4/<init>()V
       dup
       aload 1
       putfield frame_4/SL Lframe_2;
       dup
       sipush 1
       putfield frame_4/y I
       astore 1
       aload 1
       checkcast frame_4
       getfield frame_4/SL Lframe_2;
       getfield frame_2/x I
       aload 1
       checkcast frame_4
       getfield frame_4/y I
       iadd
       aload 1
       checkcast frame_4
       getfield frame_4/SL Lframe_2;
       astore 1
       iadd
       aload 1
       checkcast frame_2
       getfield frame_2/SL Lframe_1;
       astore 1
       new frame_5
       dup
       invokespecial frame_5/<init>()V
       dup
       aload 1
       putfield frame_5/SL Lframe_1;
       dup
       sipush 2
       putfield frame_5/x I
       astore 1
       aload 1
       checkcast frame_5
       getfield frame_5/x I
       aload 1
       checkcast frame_5
       getfield frame_5/SL Lframe_1;
       getfield frame_1/z I
       iadd
       aload 1
       checkcast frame_5
       getfield frame_5/SL Lframe_1;
       astore 1
       iadd
       new frame_6
       dup
       invokespecial frame_6/<init>()V
       dup
       sipush 1
       putfield frame_6/x I
       astore 1
       aload 1
       checkcast frame_6
       getfield frame_6/x I
       sipush 1
       iadd
       iadd
       aconst_null
       astore 1

       ; END


       ; convert to String;
       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
       ; call println
       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

       return

.end method
