.class public Demo
.super java/lang/Object

;
; standard initializer
.method public <init>()V
   aload_0
   invokenonvirtual java/lang/Object/<init>()V
   return
.end method

.method public static main([Ljava/lang/String;)V
       ; set limits used by this method
       .limit locals 2
       .limit stack 256

       ; setup local variables:

       ;    1 - the PrintStream object held in java.lang.System.out
       getstatic java/lang/System/out Ljava/io/PrintStream;

       ; place your bytecodes here
       ; START

       
       ; initialize new stackframe frame_1
       new frame_1
       dup
       invokespecial frame_1/<init>()V
       dup
       
       ; compile expression for id x
       sipush 1
       
       ; store value for id x
       putfield frame_1/x I
       
       ; end of store value for id x
       
       ; push the completed stackframe frame_1
       astore 1
       
       ; initialize new stackframe frame_2
       new frame_2
       dup
       invokespecial frame_2/<init>()V
       dup
       aload 1
       putfield frame_2/SL Lframe_1;
       dup
       
       ; compile expression for id y
       sipush 2
       
       ; store value for id y
       putfield frame_2/y I
       
       ; end of store value for id y
       
       ; push the completed stackframe frame_2
       astore 1
       
       ; getting id x
       aload 1
       checkcast frame_2
       getfield frame_2/SL Lframe_1;
       getfield frame_1/x I
       
       ; getting id y
       aload 1
       checkcast frame_2
       getfield frame_2/y I
       iadd
       
       ; end scope
       aload 1
       checkcast frame_2
       getfield frame_2/SL Lframe_1;
       astore 1
       
       ; end scope
       aconst_null
       astore 1

       ; END


       ; convert to String;
       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
       ; call println 
       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V

       return

.end method
